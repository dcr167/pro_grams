<!DOCTYPE html>
<html>
    <head>
        <title>CRUD</title>
        
        <link rel="stylesheet" type="text/css" media="screen" href="css/st.css" />
    
    
    </head>
    <body>
        <center>
        <hr><h1>Usuarios Registrados</h1><hr>
            <div id="container">
            <table border=1>
                <thead>
                    <tr>
                        <th colspan="1"><a href="index.php">Nuevo</a></th>
                        <th colspan="5">Lista de usuarios</th>
                    
                    </tr>
                
                </thead>
                <tbody>
                    <tr>
                        
                        <td class="sbt">Id</td>
                        <td class="sbt">Nombre</td>
                        <td class="sbt">Apellido</td>
                        <td class="sbt">Correo</td>
                        <td colspan="2" class="sbt">Operaciones</td>
                       
                    </tr>
                    <?php
                    include("conexion.php");
                    $query="SELECT * FROM users";
                    $resultado=$conexion->query($query);
                    while($row=$resultado->fetch_assoc()){
                        
                    ?>
                    <tr>
                        <td><?php echo $row['id'];?></td>
                        <td><?php echo $row['nombre'];?></td>
                        <td><?php echo $row['apellido'];?></td>
                        <td><?php echo $row['correo'];?></td>
                        <td><a href="modificar.php?id=<?php echo $row['id']; ?>">Modificar</a></td>
                        <td><a href="eliminar.php?id=<?php echo $row['id']; ?>">Eliminar</a></td>
                    
                    </tr>
                    <?php
                    }
                    ?>
                
                </tbody>
            
            </table>
            </div>
            
        
        </center>
    
  
    
    
    </body>

</html>